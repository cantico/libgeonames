<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once dirname(__FILE__).'/functions.php';

class LibGeoNames_tables {



	public static function load($tablename, $files, $method = 'fgetcsv', $decodeUtf8 = true, $sizePerLine = 1) {

		global $babDB;
		
		foreach($files as $file) {

			$progress = new bab_installProgressBar;
			$progress->setTitle(sprintf(geon_translate('Import CSV file %s'), $file));


			$filesize = filesize($file);
			$currsize = 0;
			$line = 0;

			if ($handle = fopen($file, 'r')) {
				while (($data = self::$method($handle)) !== FALSE) {

					if (null === $data) {
						continue;
					}

					foreach($data as &$value) {
						// strlen is used here instead of mb_strlen to compare with filesize
						// 1+ is for enclosures and linebreak
						$currsize += $sizePerLine + strlen($value);
						if ($decodeUtf8) {
						    $value = bab_getStringAccordingToDatabase($value, 'UTF-8');
						}
						$value = $babDB->quote($value);
					}

					$line++;
					
					self::stack($tablename, '('.implode(', ', $data).')');


					if ($line >= 500) {
						$line = 0;
						$percent = ($currsize * 100) / $filesize;
						$progress->setProgression(round($percent));
					}
				}
				fclose($handle);
			}

			$progress->setProgression(100);
		}

		self::stack($tablename);
	}



	
	private function dbip_fgetcsv($handle) {

	    if (($data = fgetcsv($handle, null, ',', '"')) !== FALSE) {
	    
	        // 0 ip_start
	        // 1 ip_end
	        // 2 country
	        
	        if (3 !== count($data)) {
	            var_dump($data);
	            throw new Exception('wrong column count');
	        }
	        
	        return array(
	            geon_addrType($data[1]),
	            inet_pton($data[0]),
	            inet_pton($data[1]),
	            $data[2]
	        );
	    }
	    
	    return false;
	}





	private function fgetcsv($handle) {

		if ('#' === fgets($handle, 2)) {
			fgetcsv($handle, 50000, "\t", "`");
			return null;
		}

		fseek($handle, ftell($handle) - 1);

		if (($data = fgetcsv($handle, 50000, "\t", "`")) !== FALSE) {

			if (1 === count($data) && '' === (string) $data[0]) {
				return false;
			}

			return $data;
		}

		return false;
	}




	/**
	 * @param	string	$table
	 * @param	string	$data		(a,b,c)
	 */
	private static function stack($table, $data = null) {
		global $babDB;
		static $storage = array();

		if (!isset($storage[$table])) {
			$storage[$table] = array();
		}


		if (null !== $data) {
			$storage[$table][] = $data;
		}


		if (null === $data || 500 < count($storage[$table])) {
			$babDB->db_query('INSERT INTO '.$babDB->backtick($table).' VALUES '.implode(", \n", $storage[$table]));
			unset($storage[$table]);
		}

	}


	/**
	 * bab_installWindow since 7.1.90
	 * @param	string	$text
	 */
	public static function display($text) {
		if (method_exists('bab_installWindow','message')) {
			bab_installWindow::message(bab_toHtml($text));
		}
	}
	
	
	
	
	
	
	public function similarities() {
		
		global $babDB;
		
		$res = $babDB->db_query('SELECT placename FROM geon_postalcode GROUP BY placename ORDER BY placename');
		$data = array();
		$length = array();
		while ($arr = $babDB->db_fetch_assoc($res)) {
			
			$length[$arr['placename']] = strlen($arr['placename']);
			
			if (4 > $length[$arr['placename']]) {
				continue;
			}
			
			$data[] = $arr['placename'];
		}
		
		$progress = new bab_installProgressBar;
		$progress->setTitle(geon_translate('Cache placenames similarities'));
		
		$total = $babDB->db_num_rows($res);
		$babDB->db_data_seek($res, 0);
		
		$i = 0;
		$line = 0;
		
		while ($arr = $babDB->db_fetch_assoc($res)) {
			
			foreach($data as $placename) {
				
				if ($arr['placename'] == $placename) {
					continue;
				}
				
				// this test can be removed to add more similarities
				if ($length[$arr['placename']] !== $length[$placename]) {
					continue;
				}
				
				$lev = levenshtein($arr['placename'], $placename);
				
				if (2 > $lev) {
					// differences beetween strings is 1 char
					
					self::stack('geon_similarities', '('.$babDB->quote($arr['placename']).','.$babDB->quote($placename).')');
				}
			}
			
			if ($line >= 500) {
				$line = 0;
				$percent = ($i * 100) / $total;
				$progress->setProgression(round($percent));
			}
			
			$line++;
			$i++;
		}
		
		self::stack('geon_similarities');
		$progress->setProgression(100);
	}
}











function LibGeoNames_install() {

	global $babDB;

	/*
	$babDB->db_query('DROP TABLE IF EXISTS '.$babDB->backtick('geon_geoname'));
	$babDB->db_query('DROP TABLE IF EXISTS '.$babDB->backtick('geon_postalcode'));
	$babDB->db_query('DROP TABLE IF EXISTS '.$babDB->backtick('geon_similarities'));
	$babDB->db_query('DROP TABLE IF EXISTS '.$babDB->backtick('geon_timezone'));
	$babDB->db_query('DROP TABLE IF EXISTS '.$babDB->backtick('geon_country'));
	*/

	include_once $GLOBALS['babInstallPath'].'utilit/devtools.php';
	
	$tables = new bab_synchronizeSql(dirname(__FILE__).'/sql/dump.sql');

	bab_setTimeLimit(3600);
	
	
	// $md5_geoname = md5_file(dirname(__FILE__).'/sources/FR/main.txt');
	$md5_postalcode = md5_file(dirname(__FILE__).'/sources/FR/postalcodes.txt');
	$md5_similarities = md5_file(dirname(__FILE__).'/sources/FR/geon_similarities.txt');
	$md5_timezone = md5_file(dirname(__FILE__).'/sources/timeZones.txt');
	$md5_country = md5_file(dirname(__FILE__).'/sources/countryInfo2.txt');
	
	$md5_dbip = md5_file(dirname(__FILE__).'/sources/dbip-country.csv');
	
	/*
	LibGeoNames_tables::display($md5_geoname);
	LibGeoNames_tables::display($md5_postalcode);
	LibGeoNames_tables::display($md5_similarities);
	LibGeoNames_tables::display($md5_timezone);
	LibGeoNames_tables::display($md5_country);
	*/
	
	/*
	if ('51576ea826bb95cddc54bec7911be2ee' !== $md5_geoname || $tables->isEmpty('geon_geoname'))
	{
		$babDB->db_query('TRUNCATE geon_geoname');
		LibGeoNames_tables::display(geon_translate('Main table install'));
		LibGeoNames_tables::load('geon_geoname', array(dirname(__FILE__).'/sources/FR/main.txt'));
	}
	*/
	$force = false;
	if (isset($_SESSION['LibGeoNamesForce']) && $_SESSION['LibGeoNamesForce']) {
		$force = true;
		unset($_SESSION['LibGeoNamesForce']);
	}
	if ('debd0ba4deef48cb287f207b60570c88' !== $md5_postalcode || $tables->isEmpty('geon_postalcode') || $force)
	{
		$babDB->db_query('TRUNCATE geon_postalcode');
		LibGeoNames_tables::display(geon_translate('Postal codes table install'));
		LibGeoNames_tables::load('geon_postalcode', array(dirname(__FILE__).'/sources/FR/postalcodes.txt'));
	}
	
	
	if ('89616faa85a3bfcc13cbd00c345082a6' !== $md5_similarities || $tables->isEmpty('geon_similarities') || $force)
	{
		$babDB->db_query('TRUNCATE geon_similarities');
		// LibGeoNames_tables::similarities();
		LibGeoNames_tables::load('geon_similarities', array(dirname(__FILE__).'/sources/FR/geon_similarities.txt'));
	}

	
	if ('40ecde4c44e9831a27a0afe64c36d23b' !== $md5_timezone || $tables->isEmpty('geon_timezone') || $force)
	{
		$babDB->db_query('TRUNCATE geon_timezone');
		LibGeoNames_tables::display(geon_translate('Time zones table install'));
		LibGeoNames_tables::load('geon_timezone', array(dirname(__FILE__).'/sources/timeZones.txt'));
	}

	if ('2f6c5e4e2ce99d612c6b6859dec3407f' !== $md5_country || $tables->isEmpty('geon_country') || $force)
	{
		$babDB->db_query('TRUNCATE geon_country');
		LibGeoNames_tables::display(geon_translate('Countries table install'));
		LibGeoNames_tables::load('geon_country', array(dirname(__FILE__).'/sources/countryInfo2.txt'));
	}
	
	
	if ('e6c0d14e7a513eb98cb75b7d165a988a' !== $md5_dbip || $tables->isEmpty('geon_lookup') || $force)
	{
	    $babDB->db_query('TRUNCATE geon_lookup');
	    LibGeoNames_tables::display(geon_translate('Ip database table install'));
	    LibGeoNames_tables::load('geon_lookup', array(dirname(__FILE__).'/sources/dbip-country.csv'), 'dbip_fgetcsv', false, 6);
	}
}



<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';


function geon_preinstall_translate($id) {

	static $strings = array(
	
		'en' => array(
			0 => 'LibOrm addon',
			1 => 'Available',
			2 => 'Not available'
		),
	
		'fr' => array(
			0 => 'Module LibOrm',
			1 => 'Disponible',
			2 => 'Non disponible'
		)
	);
	
	
	$lang = isset($GLOBALS['babLanguage']) ? $GLOBALS['babLanguage'] : 'en';

	if (!function_exists(('bab_getStringAccordingToDataBase'))) {
		return $strings[$lang][$id];
	}
	return bab_getStringAccordingToDataBase($strings[$lang][$id], 'ISO-8859-15');
}


return array(
	0 => array(
	
		'description' 		=> geon_preinstall_translate(0),
		'required' 			=> false,
		'recommended' 		=> geon_preinstall_translate(1),
		'current'			=> false !== @bab_functionality::get('LibOrm') ? geon_preinstall_translate(1) : geon_preinstall_translate(2),
		'result'			=> false !== @bab_functionality::get('LibOrm')
	
	)
);
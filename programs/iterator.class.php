<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';


require_once $GLOBALS['babInstallPath'].'utilit/iterator/iterator.php';


abstract class geon_Record extends ArrayObject {
	
	public function __construct($arr) {
		parent::__construct($arr, ArrayObject::ARRAY_AS_PROPS);
	}
}

/**
 * Country record
 */
class geon_CountryRecord extends geon_Record {

	/**
	 * Get the name of the country according to user options
	 * french or english, if user language is not french, fallback to english
	 * @return string
	 */
	public function getName() {
		$colname = 'country_en';

		if ('fr' === $GLOBALS['babLanguage']) {
			$colname = 'country_fr';
		}

		return $this->$colname;
	}
}
 


/**
 * Postal code record
 */
class geon_PostalCodeRecord extends geon_Record {

}


abstract class geon_Iterator extends BAB_MySqlResultIterator {

}



class geon_CountryIterator extends geon_Iterator {

	public function getObject($aDatas) {
		return new geon_CountryRecord($aDatas);
	}
}




class geon_PostalCodeIterator extends geon_Iterator {

	public function getObject($aDatas) {
		return new geon_PostalCodeRecord($aDatas);
	}
}

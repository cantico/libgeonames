<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

if (!bab_isUserAdministrator()) {
	die;
}

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/install.php';


/**
 * Displays the addon configuration page.
 *
 * @param string $message
 */
function LibGeoNames_configuration()
{
	$W = bab_Widgets();	
	
	
	
	$page = $W->BabPage();
	$page->addItem(
		$W->VBoxItems(
			$W->Label("Voulez vous alleger la base de donnes ?"),
			$W->Label("(Ceci n'a d'interet que pour des problematique de developpement, si vous voulez restaurer l'etat complet cliqué sur le bouton proceder au reimport complet)"),
			$W->FlowItems(
				$W->Link("Proceder a l'allegement >", '?tg=addon/LibGeoNames/configuration&idx=lightIt'),
				$W->Label('|'),
				$W->Link("Proceder au reimport complet >", '?tg=addon/LibGeoNames/configuration&idx=forceIt')
			)->setHorizontalSpacing(1,'em')
		)
	);
	
	$page->displayHtml();
}




/**
 * Saves the addon configuration.
 *
 * @param array $config
 * @return boolean
 */
function LibGeoNames_configurationLightIt()
{
	global $babDB;

	include_once $GLOBALS['babInstallPath'].'utilit/devtools.php';
	
	$tables = new bab_synchronizeSql(dirname(__FILE__).'/sql/dump.sql');

	bab_setTimeLimit(3600);
	
	$babDB->db_query('TRUNCATE geon_postalcode');
	LibGeoNames_tables::display(geon_translate('Light Postal codes table install'));
	LibGeoNames_tables::load('geon_postalcode', array(dirname(__FILE__).'/sources/light/postalcodes.txt'));
	
	$babDB->db_query('TRUNCATE geon_similarities');
	LibGeoNames_tables::display(geon_translate('Light Similarities table install'));
	LibGeoNames_tables::load('geon_similarities', array(dirname(__FILE__).'/sources/light/geon_similarities.txt'));
	
	$babDB->db_query('TRUNCATE geon_timezone');
	LibGeoNames_tables::display(geon_translate('Light Time zones table install'));
	LibGeoNames_tables::load('geon_timezone', array(dirname(__FILE__).'/sources/light/timeZones.txt'));

	$babDB->db_query('TRUNCATE geon_country');
	LibGeoNames_tables::display(geon_translate('Light Countries table install'));
	LibGeoNames_tables::load('geon_country', array(dirname(__FILE__).'/sources/light/countryInfo2.txt'));

    $babDB->db_query('TRUNCATE geon_lookup');
    LibGeoNames_tables::display(geon_translate('Light Ip database table install'));
    LibGeoNames_tables::load('geon_lookup', array(dirname(__FILE__).'/sources/light/dbip-country.csv'), 'dbip_fgetcsv', false, 6);
	
	
	global $babBody;
	
	$babBody->addNextPageMessage('Allegement effectue.');
	
	header("location: ?tg=addon/LibGeoNames/configuration");
}



$idx = bab_rp('idx');

switch($idx) {
		
	case 'forceIt':
		$addon = bab_getAddonInfosInstance('LibGeoNames');
	    $_SESSION['LibGeoNamesForce'] = 1;
		header('location: ?tg=addons&idx=upgrade&item='.$addon->getId());
		break;
		
	case 'lightIt':
	    LibGeoNames_configurationLightIt();
		break;

	default:
	case 'menu':
		LibGeoNames_configuration();
		break;
}

<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';






/*

class geon_geonameSet extends ORM_MySqlRecordSet {

	function __construct() {
		parent::__construct();

		$this->addFields(
			ORM_PkField('geonameid')				->setDescription(geon_translate('geoname id')),
			ORM_StringField('name', 200)			->setDescription(geon_translate('Name of geographical point')),
			ORM_StringField('asciiname', 200)		->setDescription(geon_translate('Name of geographical point in plain ascii characters')),
			ORM_TextField('alternatenames')			->setDescription(geon_translate('Alternate names, comma separated')),
			ORM_StringField('latitude', 10)			->setDescription(geon_translate('Latitude in decimal degrees (wgs84)')),
			ORM_StringField('longitude', 10)		->setDescription(geon_translate('Longitude in decimal degrees (wgs84)')),
			ORM_StringField('featureclass', 1)		->setDescription(geon_translate('Feature class')),
			ORM_StringField('featurecode', 10)		->setDescription(geon_translate('Feature code')),
			ORM_StringField('countrycode', 2)		->setDescription(geon_translate('Country code')),
			ORM_StringField('cc2', 60)				->setDescription(geon_translate('alternate country codes, comma separated')),
			ORM_StringField('admin1code', 20)		->setDescription(geon_translate('Code for the first administrative division')),
			ORM_StringField('admin2code', 80)		->setDescription(geon_translate('Code for the second administrative division')),
			ORM_StringField('admin3code', 20)		->setDescription(geon_translate('Code for third level administrative division')),
			ORM_StringField('admin4code', 20)		->setDescription(geon_translate('code for fourth level administrative division')),
			ORM_IntField('population')				->setDescription(geon_translate('Population')),
			ORM_IntField('elevation')				->setDescription(geon_translate('Elevation in meters')),
			ORM_IntField('gtopo30')					->setDescription(geon_translate('Average elevation of 30\'x30\' (ca 900mx900m) area in meters')),
			ORM_FkField('timezone', 'geon_TimeZoneOrmSet')->setDescription(geon_translate('Timezone')),
			ORM_DateField('modificationdate')		->setDescription(geon_translate('Date of last modification'))
		);

	}
}


class geon_geoname extends ORM_MySqlRecord { }

*/



class geon_postalcodeSet extends ORM_MySqlRecordSet {

	function __construct() {
		parent::__construct();

		global $babDB;
		$this->setBackend(new ORM_MySqlBackend($babDB));

		$this->addFields(
			ORM_StringField('countrycode', 2)		->setDescription(geon_translate('Country code')),
			ORM_StringField('postalcode', 10)		->setDescription(geon_translate('Postal code')),
			ORM_StringField('placename', 180)		->setDescription(geon_translate('Place name')),
			ORM_StringField('admin1name', 100)		->setDescription(geon_translate('1. order subdivision (state)')),
			ORM_StringField('admin1code', 20)		->setDescription(geon_translate('1. order subdivision (state) code')),
			ORM_StringField('admin2name', 100)		->setDescription(geon_translate('2. order subdivision (county/province)')),
			ORM_StringField('admin2code', 20)		->setDescription(geon_translate('2. order subdivision (county/province) code')),
			ORM_StringField('admin3name', 100)		->setDescription(geon_translate('3. order subdivision (community)')),
			ORM_StringField('latitude', 10)			->setDescription(geon_translate('Latitude in decimal degrees (wgs84)')),
			ORM_StringField('longitude', 10)		->setDescription(geon_translate('Longitude in decimal degrees (wgs84)')),
			ORM_IntField('accuracy')				->setDescription(geon_translate('Accuracy of lat/lng from 1=estimated to 6=centroid'))
		);

	}
}



class geon_postalcode extends ORM_MySqlRecord { }













class geon_timezoneSet extends ORM_MySqlRecordSet {

	function __construct() {
		parent::__construct();

		global $babDB;
		$this->setBackend(new ORM_MySqlBackend($babDB));

		$this->addFields(
			
			ORM_PkField('timezone')					->setDescription(geon_translate('Timezone identifier')),
			ORM_IntField('gmt')						->setDescription(geon_translate('gmt')),
			ORM_IntField('dst')						->setDescription(geon_translate('dst'))
		);

	}
}

class geon_timezone extends ORM_MySqlRecord { }




class geon_countrySet extends ORM_MySqlRecordSet {

	function __construct() {
		parent::__construct();

		global $babDB;
		$this->setBackend(new ORM_MySqlBackend($babDB));

		$this->addFields(
			
			ORM_StringField('iso', 2)						->setDescription(geon_translate('ISO-3166 2-letter country code')),
			ORM_StringField('iso3', 3)						->setDescription(geon_translate('ISO 3-letter')),
			ORM_IntField('isonumeric')						->setDescription(geon_translate('iso numeric')),
			ORM_StringField('fips', 2)						->setDescription(geon_translate('fips code')),
			ORM_StringField('country_en', 200)				->setDescription(geon_translate('Country English label')),
			ORM_StringField('country_fr', 200)				->setDescription(geon_translate('Country French label')),
			ORM_StringField('capital', 200)					->setDescription(geon_translate('Capital')),
			ORM_IntField('area')							->setDescription(geon_translate('Area (in square km)')),
			ORM_IntField('population')						->setDescription(geon_translate('Population')),
			ORM_StringField('continent', 2)					->setDescription(geon_translate('Continent')),
			ORM_StringField('tld', 3)						->setDescription(geon_translate('tld')),
			ORM_StringField('currencycode', 3)				->setDescription(geon_translate('Currency code')),
			ORM_StringField('currencyname', 100)			->setDescription(geon_translate('Currency name')),
			ORM_IntField('phone', 4)						->setDescription(geon_translate('Phone')),
			ORM_StringField('postalcodeformat', 255)		->setDescription(geon_translate('Postal code format')),
			ORM_StringField('postalcoderegex', 255)			->setDescription(geon_translate('Postal code regex')),
			ORM_StringField('languages', 255)				->setDescription(geon_translate('Languages')),
			ORM_IntField('geonameid')						->setDescription(geon_translate('Geoname id')),
			ORM_StringField('neighbours', 255)				->setDescription(geon_translate('Neighbours')),
			ORM_StringField('equivalentfipscode', 255)		->setDescription(geon_translate('Equivalent fips code'))
		);

	}
}


class geon_country extends ORM_MySqlRecord { }



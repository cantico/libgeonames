



--
-- The main 'geoname' table
--

CREATE TABLE `geon_geoname` (
  `geonameid` 				int(10) 		unsigned	NOT NULL 	default '0',
  `name` 					varchar(200) 				NOT NULL 	default ''				COMMENT 'name of geographical point',
  `asciiname` 				varchar(200) 				NOT NULL	default ''				COMMENT 'name of geographical point in plain ascii characters',
  `alternatenames` 			text 						NOT NULL							COMMENT 'alternatenames, comma separated',
  `latitude` 				decimal(10,7) 			 	NOT NULL	default '0.0000000'		COMMENT 'latitude in decimal degrees (wgs84)',
  `longitude` 				decimal(10,7)				NOT NULL	default '0.0000000'		COMMENT 'longitude in decimal degrees (wgs84)',
  `featureclass` 			char(1) 					NOT NULL 	default ''				COMMENT 'see http://www.geonames.org/export/codes.html',
  `featurecode` 			varchar(10) 	 			NOT NULL	default ''				COMMENT 'see http://www.geonames.org/export/codes.html',
  `countrycode` 			char(2) 					NOT NULL 	default '' 				COMMENT 'ISO-3166 2-letter country code',
  `cc2` 					varchar(60) 	 			NOT NULL	default ''				COMMENT 'alternate country codes, comma separated, ISO-3166 2-letter country code',
  `admin1code` 				varchar(20) 	 			NOT NULL	default ''				COMMENT 'fipscode (subject to change to iso code), isocode for the us and ch',
  `admin2code` 				varchar(80) 	 			NOT NULL	default ''				COMMENT 'code for the second administrative division, a county in the US',
  `admin3code` 				varchar(20) 	 			NOT NULL	default ''				COMMENT 'code for third level administrative division',
  `admin4code` 				varchar(20) 	 			NOT NULL	default ''				COMMENT 'code for fourth level administrative division',
  `population` 				bigint(20) 		unsigned 	NOT NULL 	default '0',
  `elevation` 				int(10)		 	unsigned 	NOT NULL	default '0'				COMMENT 'in meters, integer',
  `gtopo30` 				int(10)		 	unsigned 	NOT NULL	default '0'				COMMENT 'average elevation of 30''x30'' (ca 900mx900m) area in meters',
  `timezone` 				varchar(100)			 	NOT NULL	default ''				COMMENT 'link to timezone table',  
  `modificationdate` 		date 						NOT NULL	default '0000-00-00'	COMMENT 'date of last modification in yyyy-MM-dd format',
  PRIMARY KEY  (`geonameid`),
  KEY `countrycode` (`countrycode`),
  KEY `name` (`name`),
  KEY `featurecode` (`featurecode`)
);






-- postal codes

CREATE TABLE `geon_postalcode` (

  `countrycode` 			char(2) 					NOT NULL 	default '' 				COMMENT 'ISO-3166 2-letter country code',
  `postalcode` 				varchar(10) 	 			NOT NULL	default ''				COMMENT 'postal code',
  `placename` 				varchar(180) 	 			NOT NULL	default ''				COMMENT 'place name',
  `admin1name` 				varchar(100) 	 			NOT NULL	default ''				COMMENT '1. order subdivision (state)',
  `admin1code` 				varchar(20) 	 			NOT NULL	default ''				COMMENT '1. order subdivision (state)',
  `admin2name` 				varchar(100) 	 			NOT NULL	default ''				COMMENT '2. order subdivision (county/province)',
  `admin2code` 				varchar(20) 	 			NOT NULL	default ''				COMMENT '2. order subdivision (county/province)',
  `admin3name` 				varchar(100) 	 			NOT NULL	default ''				COMMENT '3. order subdivision (community)',	
  `latitude` 				decimal(10,7) 			 	NOT NULL	default '0.0000000'		COMMENT 'latitude in decimal degrees (wgs84)',
  `longitude` 				decimal(10,7)				NOT NULL	default '0.0000000'		COMMENT 'longitude in decimal degrees (wgs84)',
  `accuracy` 				tinyint(1)		unsigned	NOT NULL	default '0'				COMMENT 'accuracy of lat/lng from 1=estimated to 6=centroid',
  PRIMARY KEY  (`postalcode`, `placename`),
  KEY `position` (`latitude`, `longitude`)
);




-- similarity beetween placeames from the postal codes table

CREATE TABLE `geon_similarities` (

  `placename1` 				varchar(160) 	 			NOT NULL	default ''				COMMENT 'place name',
  `placename2` 				varchar(160) 	 			NOT NULL	default ''				COMMENT 'place name',
  PRIMARY KEY  (`placename1`, `placename2`)
);






-- TimeZones		

CREATE TABLE `geon_timezone` (
  `timezone` 				varchar(100) 				NOT NULL 	default '',
  `gmt` 					decimal(4,1) 				NOT NULL	default '0.0'			COMMENT 'GMT offset 1. Jan 2009',
  `dst` 					decimal(4,1) 				NOT NULL	default '0.0'			COMMENT 'DST offset 1. Jul 2009',
  PRIMARY KEY  (`timezone`)
);








-- Countries	
-- ISO	ISO3	ISO-Numeric	fips	Country	Capital	Area(in sq km)	Population	Continent	tld	CurrencyCode	CurrencyName	Phone	Postal Code Format	Postal Code Regex	Languages	geonameid	neighbours	EquivalentFipsCode
-- ('D', 'AND', '20', 'AN', 'Andorra', 'Andorra la Vella', '468', '72000', 'EU', '.ad', 'EUR', 'Euro', '376', 'AD###', '^(?:AD)*(\\d{3})$', 'ca,fr-AD,pt', '3041565', 'ES,FR', '')
CREATE TABLE `geon_country` (
  `iso` 					char(2) 					NOT NULL 	default '' 				COMMENT 'ISO-3166 2-letter country code',
  `iso3` 					char(3) 					NOT NULL	default '',
  `isonumeric` 				tinyint(3)		unsigned	NOT NULL	default '0',
  `fips` 					char(2) 					NOT NULL	default '',
  `country_en` 				varchar(200) 				NOT NULL	default ''				COMMENT 'English label',
  `country_fr` 				varchar(200) 				NOT NULL	default ''				COMMENT 'French label',
  `capital` 				varchar(200) 				NOT NULL	default ''				COMMENT 'English label',
  `area` 					int(10) 		unsigned	NOT NULL	default '0'				COMMENT 'Area (in square km)',
  `population` 				bigint(20)		unsigned	NOT NULL	default '0',
  `continent` 				char(2) 					NOT NULL	default '',
  `tld` 					char(3) 					NOT NULL	default '',
  `currencycode` 			char(3) 					NOT NULL	default '',
  `currencyname` 			varchar(200) 				NOT NULL	default '',
  `phone` 					tinyint(4)		unsigned	NOT NULL	default '0',
  `postalcodeformat` 		varchar(255) 				NOT NULL	default '',
  `postalcoderegex` 		varchar(255) 				NOT NULL	default '',
  `languages` 				varchar(255) 				NOT NULL	default '',
  `geonameid` 				int(10) 		unsigned	NOT NULL 	default '0',
  `neighbours` 				varchar(255) 				NOT NULL	default '',
  `equivalentfipscode`		varchar(255) 				NOT NULL	default '',

  PRIMARY KEY  (`iso`),
  UNIQUE KEY `iso3` (`iso3`),
  KEY `continent` (`continent`)
);






CREATE TABLE `geon_lookup` (
  `addr_type` enum('ipv4','ipv6') NOT NULL,
  `ip_start` varbinary(16) NOT NULL,
  `ip_end` varbinary(16) NOT NULL,
  `country` char(2) NOT NULL,
  PRIMARY KEY (`ip_start`)
);
